package mm.tm.jxcos.constant;

/**
 * Descriptions：
 * <p>
 * Author：ChenME
 * Date：2018/11/23
 * Email：ibelieve1210@163.com
 */
public interface Const {
    Boolean IsDebug = true;
    String Default_Chartest = "UTF-8";
    String Default_JsonContentType = "application/json;charset=UTF-8";
}
