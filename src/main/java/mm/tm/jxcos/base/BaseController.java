package mm.tm.jxcos.base;

import mm.tm.jxcos.comm.ResultCode;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BaseController {

    /**
     * 将得到的 list 集合处理并返回
     *
     * @param list
     * @param <T>
     * @return
     */
    protected <T> BaseResp<Map<String, Object>> handleListResult(List<T> list) {

        BaseResp<Map<String, Object>> resp = new BaseResp<>();
        Map<String, Object> map = new HashMap<>();

        if (null == list || list.size() <= 0) {
            resp.setStatus(ResultCode.ErrDataEmpty.getCode());
            resp.setMsg(ResultCode.ErrDataEmpty.getMessage());
        } else {
            map.put("list", list);
            resp.setData(map);
        }
        return resp;
    }

    /**
     * 将得到的 map 集合处理并返回
     *
     * @param map
     * @return
     */
    protected BaseResp<Map<String, Object>> handleMapResult(Map<String, Object> map) {

        BaseResp<Map<String, Object>> resp = new BaseResp<>();
        resp.setData(map);
        return resp;
    }

    /**
     * 将得到的 bean 处理并返回
     *
     * @param bean
     * @param <T>
     * @return
     */
    protected <T> BaseResp<T> handleBeanResult(T bean) {

        BaseResp<T> resp = new BaseResp<>();

        if (null == bean) {
            resp.setStatus(ResultCode.Error.getCode());
            resp.setMsg(ResultCode.Error.getMessage());
        } else {
            resp.setData(bean);
        }
        return resp;
    }

    /**
     * 将得到的 bool 处理并返回
     *
     * @param isSuccess
     * @return
     */
    protected BaseResp handleSuccess(boolean isSuccess) {

        BaseResp resp = new BaseResp();

        if (!isSuccess) {
            resp.setStatus(ResultCode.Error.getCode());
            resp.setMsg(ResultCode.Error.getMessage());
        }
        return resp;
    }
}