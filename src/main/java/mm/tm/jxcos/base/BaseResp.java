package mm.tm.jxcos.base;

import mm.tm.jxcos.comm.ResultCode;

import java.io.Serializable;

/**
 * Descriptions：
 * <p>
 * Author：ChenME
 * Date：2018/12/21
 * Email：ibelieve1210@163.com
 */
public final class BaseResp<T> implements Serializable {

    private Integer status;
    private String msg;
    private T data;

    public BaseResp() {
        this.status =  ResultCode.Success.getCode();
        this.msg = ResultCode.Success.getMessage();
    }

    public BaseResp(Integer status, String msg) {
        this.status = status;
        this.msg = msg;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}