package mm.tm.jxcos.dao;

import mm.tm.jxcos.bean.TypeFatherBean;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Descriptions：
 * <p>
 * Author：ChenME
 * Date：2018/12/21
 * Email：ibelieve1210@163.com
 */
@Component
public interface TypeFatherDao {

    /**
     * 获取所有的大类型
     */
    List<TypeFatherBean> listAllTypeFather();

    /**
     * 通过大类型的 ID 获取大类型
     */
    TypeFatherBean findTypeFatherById(Integer id);

    /**
     * 添加一个大类型
     */
    Integer addTypeFather(TypeFatherBean typeFatherBean);

    /**
     * 更新一个大类型
     */
    Integer updateTypeFather(TypeFatherBean typeFatherBean);

    /**
     * 删除一个大类型
     */
    Integer delTypeFather(Integer id);
}