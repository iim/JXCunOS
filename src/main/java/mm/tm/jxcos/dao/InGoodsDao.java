package mm.tm.jxcos.dao;

import mm.tm.jxcos.bean.InGoodsBean;
import org.springframework.stereotype.Component;

/**
 * Descriptions：入库记录
 * <p>
 * Author：ChenME
 * Date：2018/12/21
 * Email：ibelieve1210@163.com
 */
@Component
public interface InGoodsDao {

    /**
     * 新增一条入库记录
     */
    Integer addInGoods(InGoodsBean goods);
}