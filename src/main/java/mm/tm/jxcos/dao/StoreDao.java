package mm.tm.jxcos.dao;

import mm.tm.jxcos.bean.StoreBean;
import org.springframework.stereotype.Component;

/**
 * Descriptions：
 * <p>
 * Author：ChenME
 * Date：2018/12/21
 * Email：ibelieve1210@163.com
 */
@Component
public interface StoreDao {

    /**
     * 新增一条库存记录
     */
    Integer addStore(StoreBean goods);

    /**
     * 通过商品 ID 获取一条商品记录
     */
    StoreBean findStoreByGoodsId(Integer goodsId);

    /**
     * 更新一条商品记录
     */
    Integer updateStore(StoreBean goods);
}