package mm.tm.jxcos.dao;

import mm.tm.jxcos.bean.SizeBean;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Descriptions：尺寸
 * <p>
 * Author：ChenME
 * Date：2018/12/21
 * Email：ibelieve1210@163.com
 */
@Component
public interface SizeDao {

    /**
     * 根据商品的大类型 ID 获取商品的尺寸
     */
    List<SizeBean> listSizeByTypeFatherId(Integer typeFatherId);

    /**
     * 更新尺寸
     */
    Integer updateSize(SizeBean bean);

    /**
     * 新增一个尺寸
     */
    Integer addSize(SizeBean bean);
}