package mm.tm.jxcos.dao;

import mm.tm.jxcos.bean.ColorBean;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Descriptions：商品颜色
 * <p>
 * Author：ChenME
 * Date：2018/12/21
 * Email：ibelieve1210@163.com
 */
@Component
public interface ColorDao {

    /**
     * 获取商品的所有颜色
     */
    List<ColorBean> listAllColor();

    /**
     * 更新颜色
     *
     * @return
     */
    Integer updateColor(ColorBean bean);


    /**
     * 新增一个颜色
     */
    Integer addColor(ColorBean bean);
}