package mm.tm.jxcos.dao;

import mm.tm.jxcos.bean.TypeSonBean;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Descriptions：
 * <p>
 * Author：ChenME
 * Date：2018/12/21
 * Email：ibelieve1210@163.com
 */
@Component
public interface TypeSonDao {

    /**
     * 根据商品的大类型 ID 获取商品的子类型类型（毛衣、外套）
     */
    List<TypeSonBean> listTypeSonByFatherId(Integer typeFatherId);

    /**
     * 更新类型
     */
    Integer updateTypeSon(TypeSonBean bean);

    /**
     * 新增一个类型
     */
    Integer addTypeSon(TypeSonBean bean);
}