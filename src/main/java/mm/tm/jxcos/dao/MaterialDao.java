package mm.tm.jxcos.dao;

import mm.tm.jxcos.bean.MaterialBean;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Descriptions：商品材质
 * <p>
 * Author：ChenME
 * Date：2018/12/21
 * Email：ibelieve1210@163.com
 */
@Component
public interface MaterialDao {

    /**
     * 获取商品的所有材质
     */
    List<MaterialBean> listAllMaterial();

    /**
     * 更新材质
     *
     * @return
     */
    Integer updateMaterial(MaterialBean bean);


    /**
     * 新增一个材质
     */
    Integer addMaterial(MaterialBean bean);
}