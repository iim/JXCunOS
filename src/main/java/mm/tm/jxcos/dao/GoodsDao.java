package mm.tm.jxcos.dao;

import mm.tm.jxcos.bean.GoodsBean;
import mm.tm.jxcos.domain.ListGoodsBySelectReq;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * Descriptions：商品
 * <p>
 * Author：ChenME
 * Date：2018/12/21
 * Email：ibelieve1210@163.com
 */
@Component
public interface GoodsDao {

    /**
     * 新增商品
     */
    Integer addGoods(GoodsBean goods);

    /**
     * 获取库存列表
     */
    List<Map<String, Object>> listStoreBySelect(ListGoodsBySelectReq req);

    /**
     * 通过 goodsId 查找商品
     */
    Map<String, Object> findGoodsById(Integer goodsId);
}