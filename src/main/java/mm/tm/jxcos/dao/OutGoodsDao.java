package mm.tm.jxcos.dao;

import mm.tm.jxcos.bean.OutGoodsBean;
import mm.tm.jxcos.domain.ListOutGoodsRecordReq;
import mm.tm.jxcos.domain.UpdatePayStatusInOutRecordReq;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * Descriptions：
 * <p>
 * Author：ChenME
 * Date：2018/12/21
 * Email：ibelieve1210@163.com
 */
@Component
public interface OutGoodsDao {

    /**
     * 增加一条出库记录
     */
    Integer addOutRecord(OutGoodsBean outGoods);

    /**
     * 通过筛选获取出库记录
     */
    List<Map<String, Object>> listAllOutRecordBySelect(ListOutGoodsRecordReq req);

    /**
     * 更新支付状态
     */
    Integer updatePayStatusByOutId(UpdatePayStatusInOutRecordReq req);
}