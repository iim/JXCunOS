package mm.tm.jxcos.domain;

/**
 * Descriptions：
 * <p>
 * Author：ChenME
 * Date：2018/12/21
 * Email：ibelieve1210@163.com
 */
public class OutGoodsFormStoreReq {
    private Integer goodsId; // : Int,
    private Integer outNumber; // : Int,
    private Integer priceOut; // : Int,
    private Integer payStatus; // : Int,
    private String outRemark; // : String?

    public OutGoodsFormStoreReq() {
    }

    public Integer getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }

    public Integer getOutNumber() {
        return outNumber;
    }

    public void setOutNumber(Integer outNumber) {
        this.outNumber = outNumber;
    }

    public Integer getPriceOut() {
        return priceOut;
    }

    public void setPriceOut(Integer priceOut) {
        this.priceOut = priceOut;
    }

    public Integer getPayStatus() {
        return payStatus;
    }

    public void setPayStatus(Integer payStatus) {
        this.payStatus = payStatus;
    }

    public String getOutRemark() {
        return outRemark;
    }

    public void setOutRemark(String outRemark) {
        this.outRemark = outRemark;
    }
}
