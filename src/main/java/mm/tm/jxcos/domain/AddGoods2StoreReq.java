package mm.tm.jxcos.domain;

/**
 * Descriptions：
 * <p>
 * Author：ChenME
 * Date：2018/12/21
 * Email：ibelieve1210@163.com
 */
public class AddGoods2StoreReq {
    private Integer id; //  : Int = 0,
    private String name; //  : String,
    private String goodsNumber; //  : String,
    private Integer typeFather; //  : Int,
    private Integer type; //  : Int,
    private Integer color; //  : Int,
    private Integer size; //  : Int,
    private Integer material; //  : Int,
    private Integer gender; //  : Int,
    private Long priceBuying; //  : Int,
    private Long priceGuidance; //  : Int = 0,
    private String description; //  : String?,
    private String fromBusiness; //  : String?,
    private String remark; //  : String?,
    private Integer inNumber; //  : Int

    public AddGoods2StoreReq() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGoodsNumber() {
        return goodsNumber;
    }

    public void setGoodsNumber(String goodsNumber) {
        this.goodsNumber = goodsNumber;
    }

    public Integer getTypeFather() {
        return typeFather;
    }

    public void setTypeFather(Integer typeFather) {
        this.typeFather = typeFather;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getColor() {
        return color;
    }

    public void setColor(Integer color) {
        this.color = color;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public Integer getMaterial() {
        return material;
    }

    public void setMaterial(Integer material) {
        this.material = material;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public Long getPriceBuying() {
        return priceBuying;
    }

    public void setPriceBuying(Long priceBuying) {
        this.priceBuying = priceBuying;
    }

    public Long getPriceGuidance() {
        return priceGuidance;
    }

    public void setPriceGuidance(Long priceGuidance) {
        this.priceGuidance = priceGuidance;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFromBusiness() {
        return fromBusiness;
    }

    public void setFromBusiness(String fromBusiness) {
        this.fromBusiness = fromBusiness;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getInNumber() {
        return inNumber;
    }

    public void setInNumber(Integer inNumber) {
        this.inNumber = inNumber;
    }
}
