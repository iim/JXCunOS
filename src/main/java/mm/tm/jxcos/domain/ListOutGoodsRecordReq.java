package mm.tm.jxcos.domain;

/**
 * Descriptions：
 * <p>
 * Author：ChenME
 * Date：2018/12/21
 * Email：ibelieve1210@163.com
 */
public class ListOutGoodsRecordReq {
    private Integer payStatus; // 支付状态（0 全部，1 待支付，2 已支付）

    public ListOutGoodsRecordReq() {
        this.payStatus = 1;
    }

    public Integer getPayStatus() {
        return payStatus;
    }

    public void setPayStatus(Integer payStatus) {
        this.payStatus = payStatus;
    }
}
