package mm.tm.jxcos.domain;

import java.util.Date;

/**
 * Descriptions：
 * <p>
 * Author：ChenME
 * Date：2018/12/21
 * Email：ibelieve1210@163.com
 */
public class UpdatePayStatusInOutRecordReq {
    private Integer outId;  //  : Int = -1,
    private Integer payStatus;  //  : Int = 0,
    private Date lastModifyTime;  //  : Date?,
    private String outRemark;  //  : String?

    public UpdatePayStatusInOutRecordReq() {
    }

    public Integer getOutId() {
        return outId;
    }

    public void setOutId(Integer outId) {
        this.outId = outId;
    }

    public Integer getPayStatus() {
        return payStatus;
    }

    public void setPayStatus(Integer payStatus) {
        this.payStatus = payStatus;
    }

    public Date getLastModifyTime() {
        return lastModifyTime;
    }

    public void setLastModifyTime(Date lastModifyTime) {
        this.lastModifyTime = lastModifyTime;
    }

    public String getOutRemark() {
        return outRemark;
    }

    public void setOutRemark(String outRemark) {
        this.outRemark = outRemark;
    }
}
