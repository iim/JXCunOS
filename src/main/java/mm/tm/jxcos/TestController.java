package mm.tm.jxcos;

import mm.tm.jxcos.constant.Const;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Descriptions：
 * <p>
 * Author：ChenME
 * Date：2018/11/23
 * Email：ibelieve1210@163.com
 */

@RestController
@RequestMapping(produces = {Const.Default_JsonContentType}, value = {"/test"})
public class TestController {

    @GetMapping("/hello")
    public String hello(){
        return "Hello";
    }
}
