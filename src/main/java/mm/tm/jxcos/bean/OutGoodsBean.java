package mm.tm.jxcos.bean;

import java.util.Date;

/**
 * Descriptions：
 * <p>
 * Author：ChenME
 * Date：2018/12/21
 * Email：ibelieve1210@163.com
 */
public class OutGoodsBean {

    private Integer outId;  //   : Int = 0,
    private Integer goodsId;  //   : Int,
    private Date outTime;  //   : Date,
    private Integer outNumber;  //   : Int,
    private Integer priceOut;  //   : Int,
    private Integer payStatus;  //   : Int,
    private Date lastModifyTime;  //   : Date,
    private String outRemark;  //   : String?

    public OutGoodsBean() {
    }

    public OutGoodsBean(Integer outId, Integer goodsId, Date outTime, Integer outNumber, Integer priceOut, Integer payStatus, Date lastModifyTime, String outRemark) {
        this.outId = outId;
        this.goodsId = goodsId;
        this.outTime = outTime;
        this.outNumber = outNumber;
        this.priceOut = priceOut;
        this.payStatus = payStatus;
        this.lastModifyTime = lastModifyTime;
        this.outRemark = outRemark;
    }

    public Integer getOutId() {
        return outId;
    }

    public void setOutId(Integer outId) {
        this.outId = outId;
    }

    public Integer getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }

    public Date getOutTime() {
        return outTime;
    }

    public void setOutTime(Date outTime) {
        this.outTime = outTime;
    }

    public Integer getOutNumber() {
        return outNumber;
    }

    public void setOutNumber(Integer outNumber) {
        this.outNumber = outNumber;
    }

    public Integer getPriceOut() {
        return priceOut;
    }

    public void setPriceOut(Integer priceOut) {
        this.priceOut = priceOut;
    }

    public Integer getPayStatus() {
        return payStatus;
    }

    public void setPayStatus(Integer payStatus) {
        this.payStatus = payStatus;
    }

    public Date getLastModifyTime() {
        return lastModifyTime;
    }

    public void setLastModifyTime(Date lastModifyTime) {
        this.lastModifyTime = lastModifyTime;
    }

    public String getOutRemark() {
        return outRemark;
    }

    public void setOutRemark(String outRemark) {
        this.outRemark = outRemark;
    }
}
