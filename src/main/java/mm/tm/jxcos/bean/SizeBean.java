package mm.tm.jxcos.bean;

/**
 * Descriptions：
 * <p>
 * Author：ChenME
 * Date：2018/12/21
 * Email：ibelieve1210@163.com
 */
public class SizeBean {
    private Integer sizeId;  //  : Int = 0,
    private Integer typeFatherId;  //  : Int, // 商品类别的ID
    private String sizeName;  //  : String,
    private String sizeDescription;  //  : String?,
    private String sizeRemark;  //  : String?

    public SizeBean() {
    }

    public Integer getSizeId() {
        return sizeId;
    }

    public void setSizeId(Integer sizeId) {
        this.sizeId = sizeId;
    }

    public Integer getTypeFatherId() {
        return typeFatherId;
    }

    public void setTypeFatherId(Integer typeFatherId) {
        this.typeFatherId = typeFatherId;
    }

    public String getSizeName() {
        return sizeName;
    }

    public void setSizeName(String sizeName) {
        this.sizeName = sizeName;
    }

    public String getSizeDescription() {
        return sizeDescription;
    }

    public void setSizeDescription(String sizeDescription) {
        this.sizeDescription = sizeDescription;
    }

    public String getSizeRemark() {
        return sizeRemark;
    }

    public void setSizeRemark(String sizeRemark) {
        this.sizeRemark = sizeRemark;
    }
}
