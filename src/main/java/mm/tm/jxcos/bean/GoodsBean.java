package mm.tm.jxcos.bean;

/**
 * Descriptions：
 * <p>
 * Author：ChenME
 * Date：2018/12/21
 * Email：ibelieve1210@163.com
 */
public class GoodsBean {
    private Integer id;
    private String name;
    private String goodsNumber; // 货号
    private Integer typeFather; // 大类型 ID
    private Integer type; // 小类型 ID
    private Integer color; // 颜色 ID
    private Integer size; // 尺寸 ID
    private Integer material; // 材质 ID
    private Integer gender; // 男女款 ID
    private Long priceBuying; // 进价（单位：分）
    private Long priceGuidance; // 指导价（单位：分）
    private String description; //  描述信息
    private String fromBusiness; // 进货商
    private String remark; // 备注信息

    public GoodsBean() {
    }

    public GoodsBean(Integer id, String name, String goodsNumber, Integer typeFather, Integer type, Integer color, Integer size, Integer material, Integer gender, Long priceBuying, Long priceGuidance, String description, String fromBusiness, String remark) {
        this.id = id;
        this.name = name;
        this.goodsNumber = goodsNumber;
        this.typeFather = typeFather;
        this.type = type;
        this.color = color;
        this.size = size;
        this.material = material;
        this.gender = gender;
        this.priceBuying = priceBuying;
        this.priceGuidance = priceGuidance;
        this.description = description;
        this.fromBusiness = fromBusiness;
        this.remark = remark;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGoodsNumber() {
        return goodsNumber;
    }

    public void setGoodsNumber(String goodsNumber) {
        this.goodsNumber = goodsNumber;
    }

    public Integer getTypeFather() {
        return typeFather;
    }

    public void setTypeFather(Integer typeFather) {
        this.typeFather = typeFather;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getColor() {
        return color;
    }

    public void setColor(Integer color) {
        this.color = color;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public Integer getMaterial() {
        return material;
    }

    public void setMaterial(Integer material) {
        this.material = material;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public Long getPriceBuying() {
        return priceBuying;
    }

    public void setPriceBuying(Long priceBuying) {
        this.priceBuying = priceBuying;
    }

    public Long getPriceGuidance() {
        return priceGuidance;
    }

    public void setPriceGuidance(Long priceGuidance) {
        this.priceGuidance = priceGuidance;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFromBusiness() {
        return fromBusiness;
    }

    public void setFromBusiness(String fromBusiness) {
        this.fromBusiness = fromBusiness;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
