package mm.tm.jxcos.bean;

/**
 * Descriptions：
 * <p>
 * Author：ChenME
 * Date：2018/12/21
 * Email：ibelieve1210@163.com
 */
public class TypeFatherBean {
    private Integer typeFatherId; //  : Int = 0,
    private String typeFatherName; //  : String,
    private String typeFatherDescription; //  : String?,
    private String typeFatherRemark; //  : String?

    public TypeFatherBean() {
    }

    public Integer getTypeFatherId() {
        return typeFatherId;
    }

    public void setTypeFatherId(Integer typeFatherId) {
        this.typeFatherId = typeFatherId;
    }

    public String getTypeFatherName() {
        return typeFatherName;
    }

    public void setTypeFatherName(String typeFatherName) {
        this.typeFatherName = typeFatherName;
    }

    public String getTypeFatherDescription() {
        return typeFatherDescription;
    }

    public void setTypeFatherDescription(String typeFatherDescription) {
        this.typeFatherDescription = typeFatherDescription;
    }

    public String getTypeFatherRemark() {
        return typeFatherRemark;
    }

    public void setTypeFatherRemark(String typeFatherRemark) {
        this.typeFatherRemark = typeFatherRemark;
    }
}
