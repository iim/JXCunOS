package mm.tm.jxcos.bean;

/**
 * Descriptions：
 * <p>
 * Author：ChenME
 * Date：2018/12/21
 * Email：ibelieve1210@163.com
 */
public class MaterialBean {
    private Integer materialId;  //   : Int = 0,
    private String materialName;  //   : String,
    private String materialDescription;  //   : String?,
    private String materialRemark;  //   : String?

    public MaterialBean() {
    }

    public Integer getMaterialId() {
        return materialId;
    }

    public void setMaterialId(Integer materialId) {
        this.materialId = materialId;
    }

    public String getMaterialName() {
        return materialName;
    }

    public void setMaterialName(String materialName) {
        this.materialName = materialName;
    }

    public String getMaterialDescription() {
        return materialDescription;
    }

    public void setMaterialDescription(String materialDescription) {
        this.materialDescription = materialDescription;
    }

    public String getMaterialRemark() {
        return materialRemark;
    }

    public void setMaterialRemark(String materialRemark) {
        this.materialRemark = materialRemark;
    }
}
