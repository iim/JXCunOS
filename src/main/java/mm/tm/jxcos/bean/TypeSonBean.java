package mm.tm.jxcos.bean;

/**
 * Descriptions：
 * <p>
 * Author：ChenME
 * Date：2018/12/21
 * Email：ibelieve1210@163.com
 */
public class TypeSonBean {
    private Integer typeId;  //   : Int = 0,
    private Integer typeFatherId;  //   : Int,
    private String typeName;  //   : String ,
    private String typeDescription;  //   : String?,
    private String typeRemark;  //   : String?

    public TypeSonBean() {
    }

    public Integer getTypeId() {
        return typeId;
    }

    public void setTypeId(Integer typeId) {
        this.typeId = typeId;
    }

    public Integer getTypeFatherId() {
        return typeFatherId;
    }

    public void setTypeFatherId(Integer typeFatherId) {
        this.typeFatherId = typeFatherId;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getTypeDescription() {
        return typeDescription;
    }

    public void setTypeDescription(String typeDescription) {
        this.typeDescription = typeDescription;
    }

    public String getTypeRemark() {
        return typeRemark;
    }

    public void setTypeRemark(String typeRemark) {
        this.typeRemark = typeRemark;
    }
}
