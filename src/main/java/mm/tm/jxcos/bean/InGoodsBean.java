package mm.tm.jxcos.bean;

import java.util.Date;

/**
 * Descriptions：
 * <p>
 * Author：ChenME
 * Date：2018/12/21
 * Email：ibelieve1210@163.com
 */
public class InGoodsBean {
    private Integer inId; // : Int,
    private Integer goodsId; // : Int,
    private Date inTime; // : Date,
    private Integer inNumber; // : Int,
    private String inRemark; // : String?

    public InGoodsBean() {
    }

    public InGoodsBean(Integer inId, Integer goodsId, Date inTime, Integer inNumber, String inRemark) {
        this.inId = inId;
        this.goodsId = goodsId;
        this.inTime = inTime;
        this.inNumber = inNumber;
        this.inRemark = inRemark;
    }

    public Integer getInId() {
        return inId;
    }

    public void setInId(Integer inId) {
        this.inId = inId;
    }

    public Integer getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }

    public Date getInTime() {
        return inTime;
    }

    public void setInTime(Date inTime) {
        this.inTime = inTime;
    }

    public Integer getInNumber() {
        return inNumber;
    }

    public void setInNumber(Integer inNumber) {
        this.inNumber = inNumber;
    }

    public String getInRemark() {
        return inRemark;
    }

    public void setInRemark(String inRemark) {
        this.inRemark = inRemark;
    }
}
