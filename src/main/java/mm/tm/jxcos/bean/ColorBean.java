package mm.tm.jxcos.bean;

/**
 * Descriptions：
 * <p>
 * Author：ChenME
 * Date：2018/12/21
 * Email：ibelieve1210@163.com
 */
public class ColorBean {

    private Integer colorId; //: Int = 0,
    private String colorName; //: String,
    private String colorDescription; //: String?,
    private String colorRemark; //: String?

    public ColorBean() {
    }

    public ColorBean(String colorName, String colorDescription, String colorRemark) {
        this.colorId = 0;
        this.colorName = colorName;
        this.colorDescription = colorDescription;
        this.colorRemark = colorRemark;
    }

    public Integer getColorId() {
        return colorId;
    }

    public void setColorId(Integer colorId) {
        this.colorId = colorId;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public String getColorDescription() {
        return colorDescription;
    }

    public void setColorDescription(String colorDescription) {
        this.colorDescription = colorDescription;
    }

    public String getColorRemark() {
        return colorRemark;
    }

    public void setColorRemark(String colorRemark) {
        this.colorRemark = colorRemark;
    }
}
