package mm.tm.jxcos.bean;

import java.util.Date;

/**
 * Descriptions：
 * <p>
 * Author：ChenME
 * Date：2018/12/21
 * Email：ibelieve1210@163.com
 */
public class StoreBean {
    private Integer goodsId;  // : Int = 0,
    private Integer countLeft;  // : Int = 0,
    private Date createTime;  // : Date = Date(), // 创建时间，也就是入库时间
    private Date lastModifyTime;  // : Date = Date() // 最近操作时间

    public StoreBean() {
    }

    public StoreBean(Integer goodsId, Integer countLeft, Date createTime, Date lastModifyTime) {
        this.goodsId = goodsId;
        this.countLeft = countLeft;
        this.createTime = createTime;
        this.lastModifyTime = lastModifyTime;
    }

    public Integer getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }

    public Integer getCountLeft() {
        return countLeft;
    }

    public void setCountLeft(Integer countLeft) {
        this.countLeft = countLeft;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getLastModifyTime() {
        return lastModifyTime;
    }

    public void setLastModifyTime(Date lastModifyTime) {
        this.lastModifyTime = lastModifyTime;
    }
}
