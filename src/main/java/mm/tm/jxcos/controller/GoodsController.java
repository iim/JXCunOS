package mm.tm.jxcos.controller;

import mm.tm.jxcos.base.BaseController;
import mm.tm.jxcos.base.BaseResp;
import mm.tm.jxcos.constant.Const;
import mm.tm.jxcos.domain.*;
import mm.tm.jxcos.service.GoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * Descriptions：
 * <p>
 * Author：ChenME
 * Date：2018/11/23
 * Email：ibelieve1210@163.com
 */

@RestController
@RequestMapping(produces = {Const.Default_JsonContentType}, value = {"/goods"})
public class GoodsController extends BaseController {

    @Autowired
    GoodsService mService;

    /**
     * 根据商品的父类型 ID 获取商品的子类型类型（毛衣、外套）
     *
     * @return
     */
    @GetMapping("/listTypeSonByFatherId")
    public BaseResp<Map<String, Object>> listTypeSonByFatherId(Integer typeFatherId) {
        return handleListResult(mService.listTypeSonByFatherId(typeFatherId));
    }

    /**
     * 获取商品的所有颜色
     *
     * @return
     */
    @GetMapping("/listAllColor")
    public BaseResp<Map<String, Object>> listAllColor() {
        return handleListResult(mService.listAllColor());
    }

    /**
     * 根据商品的大类型获取商品的尺寸
     *
     * @return
     */
    @GetMapping("/listSizeByTypeFatherId")
    public BaseResp<Map<String, Object>> listSizeByTypeFatherId(Integer typeFatherId) {
        return handleListResult(mService.listSizeByTypeFatherId(typeFatherId));
    }

    /**
     * 获取商品的所有材质
     *
     * @return
     */
    @GetMapping("/listAllMaterial")
    public BaseResp<Map<String, Object>> listAllMaterial() {
        return handleListResult(mService.listAllMaterial());
    }


    /**
     * 入库
     *
     * @param req
     * @return
     */
    @PostMapping("/addGoods2Store")
    public BaseResp<Map<String, Object>> addGoods2Store(@RequestBody AddGoods2StoreReq req) {
        return handleMapResult(mService.addGoods2Store(req));
    }

    /**
     * 获取库存列表
     *
     * @return
     */
    @PostMapping("/listStoreBySelect")
    public BaseResp<Map<String, Object>> listStoreBySelect(@RequestBody ListGoodsBySelectReq req) {
        return handleListResult(mService.listStoreBySelect(req));
    }

    /**
     * 出库
     *
     * @return
     */
    @PostMapping("/outGoodsFormStore")
    public BaseResp<Map<String, Object>> outGoodsFormStore(@RequestBody OutGoodsFormStoreReq req) {
        return handleMapResult(mService.outGoodsFormStore(req));
    }

    /**
     * 通过 goodsId 查找商品
     *
     * @return
     */
    @GetMapping("/findGoodsById")
    public BaseResp<Map<String, Object>> findGoodsById(Integer goodsId) {
        return handleMapResult(mService.findGoodsById(goodsId));
    }

    /**
     * 通过筛选获取出库记录
     *
     * @return
     */
    @PostMapping("/listAllOutRecordBySelect")
    public BaseResp<Map<String, Object>> listAllOutRecordBySelect(@RequestBody ListOutGoodsRecordReq req) {
        return handleListResult(mService.listAllOutRecordBySelect(req));
    }


    /**
     * 更新支付状态
     *
     * @return
     */
    @PostMapping("/updatePayStatusByOutId")
    public BaseResp updatePayStatusByOutId(@RequestBody UpdatePayStatusInOutRecordReq req) {
        return handleSuccess(mService.updatePayStatusByOutId(req));
    }
}