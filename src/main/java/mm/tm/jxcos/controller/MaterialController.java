package mm.tm.jxcos.controller;

import mm.tm.jxcos.base.BaseController;
import mm.tm.jxcos.base.BaseResp;
import mm.tm.jxcos.bean.MaterialBean;
import mm.tm.jxcos.constant.Const;
import mm.tm.jxcos.service.MaterialService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Descriptions：
 * <p>
 * Author：ChenME
 * Date：2018/12/3
 * Email：ibelieve1210@163.com
 */

@RestController
@RequestMapping(produces = {Const.Default_JsonContentType}, value = {"/material"})
public class MaterialController extends BaseController {

    @Autowired
    MaterialService mService;


    /**
     * 更新颜色
     *
     * @return
     */
    @PostMapping("/updateMaterial")
    public BaseResp updateMaterial(@RequestBody MaterialBean req) {
        return handleSuccess(mService.updateMaterial(req));
    }

    /**
     * 新增一个颜色
     *
     * @param bean
     * @return
     */
    @PostMapping("/addMaterial")
    public BaseResp<MaterialBean> addMaterial(@RequestBody MaterialBean bean) {
        return handleBeanResult(mService.addMaterial(bean));
    }
}
