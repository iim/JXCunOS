package mm.tm.jxcos.controller;

import mm.tm.jxcos.base.BaseController;
import mm.tm.jxcos.base.BaseResp;
import mm.tm.jxcos.bean.TypeSonBean;
import mm.tm.jxcos.constant.Const;
import mm.tm.jxcos.service.TypeSonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Descriptions：
 * <p>
 * Author：ChenME
 * Date：2018/12/3
 * Email：ibelieve1210@163.com
 */

@RestController
@RequestMapping(produces = {Const.Default_JsonContentType}, value = {"/type"})
public class TypeSonController extends BaseController {

    @Autowired
    TypeSonService mService;


    /**
     * 更新类型
     *
     * @return
     */
    @PostMapping("/updateTypeSon")
    public BaseResp updateTypeSon(@RequestBody TypeSonBean req) {
        return handleSuccess(mService.updateTypeSon(req));
    }

    /**
     * 新增一个类型
     *
     * @param bean
     * @return
     */
    @PostMapping("/addTypeSon")
    public BaseResp<TypeSonBean> addTypeSon(@RequestBody TypeSonBean bean) {
        return handleBeanResult(mService.addTypeSon(bean));
    }
}
