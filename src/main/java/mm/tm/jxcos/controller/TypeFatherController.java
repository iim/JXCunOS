package mm.tm.jxcos.controller;

import mm.tm.jxcos.base.BaseController;
import mm.tm.jxcos.base.BaseResp;
import mm.tm.jxcos.bean.TypeFatherBean;
import mm.tm.jxcos.constant.Const;
import mm.tm.jxcos.domain.CommIdReq;
import mm.tm.jxcos.service.TypeFatherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * Descriptions：
 * <p>
 * Author：ChenME
 * Date：2018/11/27
 * Email：ibelieve1210@163.com
 */

@RestController
@RequestMapping(produces = {Const.Default_JsonContentType}, value = {"/type"})
public class TypeFatherController extends BaseController {

    @Autowired
    private TypeFatherService mService;

    /**
     * 获取所有的大类型
     */
    @GetMapping("/listAllTypeFather")
    public BaseResp<Map<String, Object>> listAllTypeFather() {
        return handleListResult(mService.listAllTypeFather());
    }

    /**
     * 通过大类型的 ID 获取大类型
     */
    @GetMapping("/findTypeFatherById")
    public BaseResp<TypeFatherBean> findTypeFatherById(int id) {
        return handleBeanResult(mService.findTypeFatherById(id));
    }

    /**
     * 添加一个大类型
     */
    @PostMapping("/addTypeFather")
    public BaseResp<TypeFatherBean> addTypeFather(@RequestBody TypeFatherBean typeFather) {
        return handleBeanResult(mService.addTypeFather(typeFather));
    }

    /**
     * 更新一个大类型
     */
    @PostMapping("/updateTypeFather")
    public BaseResp updateTypeFather(@RequestBody TypeFatherBean typeFather) {
        return handleSuccess(mService.updateTypeFather(typeFather));
    }

    /**
     * 删除一个大类型
     */
    @PostMapping("/delTypeFather")
    public BaseResp delTypeFather(@RequestBody CommIdReq req) {
        System.out.println(req.toString());
        return handleSuccess(mService.delTypeFather(req.getId()));
    }
}
