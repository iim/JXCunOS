package mm.tm.jxcos.controller;

import mm.tm.jxcos.base.BaseController;
import mm.tm.jxcos.base.BaseResp;
import mm.tm.jxcos.bean.ColorBean;
import mm.tm.jxcos.constant.Const;
import mm.tm.jxcos.service.ColorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Descriptions：
 * <p>
 * Author：ChenME
 * Date：2018/12/3
 * Email：ibelieve1210@163.com
 */

@RestController
@RequestMapping(produces = {Const.Default_JsonContentType}, value = {"/color"})
public class ColorController extends BaseController {

    @Autowired
    ColorService mService;


    /**
     * 更新颜色
     *
     * @return
     */
    @PostMapping("/updateColor")
    public BaseResp updateColor(@RequestBody ColorBean req) {
        return handleSuccess(mService.updateColor(req));
    }

    /**
     * 新增一个颜色
     *
     * @param bean
     * @return
     */
    @PostMapping("/addColor")
    public BaseResp<ColorBean> addColor(@RequestBody ColorBean bean) {
        return handleBeanResult(mService.addColor(bean));
    }
}
