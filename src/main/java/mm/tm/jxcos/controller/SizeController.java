package mm.tm.jxcos.controller;

import mm.tm.jxcos.base.BaseController;
import mm.tm.jxcos.base.BaseResp;
import mm.tm.jxcos.bean.SizeBean;
import mm.tm.jxcos.constant.Const;
import mm.tm.jxcos.service.SizeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Descriptions：
 * <p>
 * Author：ChenME
 * Date：2018/12/3
 * Email：ibelieve1210@163.com
 */

@RestController
@RequestMapping(produces = {Const.Default_JsonContentType}, value = {"/size"})
public class SizeController extends BaseController {

    @Autowired
    SizeService mService;


    /**
     * 更新类型
     *
     * @return
     */
    @PostMapping("/updateSize")
    public BaseResp updateSize(@RequestBody SizeBean req) {
        return handleSuccess(mService.updateSize(req));
    }

    /**
     * 新增一个类型
     *
     * @param bean
     * @return
     */
    @PostMapping("/addSize")
    public BaseResp<SizeBean> addSize(@RequestBody SizeBean bean) {
        return handleBeanResult(mService.addSize(bean));
    }
}
