package mm.tm.jxcos.comm;

/**
 * Descriptions：
 * <p>
 * Author：ChenME
 * Date：2018/12/21
 * Email：ibelieve1210@163.com
 */
public enum ResultCode {

    Error(-1, "网络请求错误"),
    ErrDataEmpty(1000, "数据为空"),
    Success(0, "成功");


    private Integer code;
    private String message;

    ResultCode(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
