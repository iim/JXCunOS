package mm.tm.jxcos.handler;

import mm.tm.jxcos.base.BaseResp;
import mm.tm.jxcos.comm.ResultCode;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;

/**
 * Descriptions：
 * <p>
 * Author：ChenME
 * Date：2018/11/27
 * Email：ibelieve1210@163.com
 */

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public BaseResp<HashMap<String, Object>> exceptionHandler(HttpServletRequest req, Exception e) throws Exception {
        BaseResp<HashMap<String, Object>> resp = new BaseResp<>();
        resp.setStatus(ResultCode.Error.getCode());
        resp.setMsg(e.getMessage());
        return resp;
    }
}
