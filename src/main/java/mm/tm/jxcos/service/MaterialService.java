package mm.tm.jxcos.service;

import mm.tm.jxcos.bean.MaterialBean;

/**
 * Descriptions：
 * <p>
 * Author：ChenME
 * Date：2018/12/21
 * Email：ibelieve1210@163.com
 */
public interface MaterialService {

    /**
     * 更新材质
     *
     * @return
     */
    Boolean updateMaterial(MaterialBean bean);


    /**
     * 新增一个材质
     */
    MaterialBean addMaterial(MaterialBean bean);
}