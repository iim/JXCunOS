package mm.tm.jxcos.service;

import mm.tm.jxcos.bean.ColorBean;
import mm.tm.jxcos.bean.MaterialBean;
import mm.tm.jxcos.bean.SizeBean;
import mm.tm.jxcos.bean.TypeSonBean;
import mm.tm.jxcos.domain.*;

import java.util.List;
import java.util.Map;

/**
 * Descriptions：
 * <p>
 * Author：ChenME
 * Date：2018/12/21
 * Email：ibelieve1210@163.com
 */
public interface GoodsService {

    /**
     * 根据商品的父类型 ID 获取商品的子类型类型（毛衣、外套）
     *
     * @return
     */
    List<TypeSonBean> listTypeSonByFatherId(Integer typeFatherId);

    /**
     * 获取商品的所有颜色
     *
     * @return
     */
    List<ColorBean> listAllColor();

    /**
     * 根据商品的大类型获取商品的尺寸
     *
     * @return
     */
    List<SizeBean> listSizeByTypeFatherId(Integer typeFatherId);

    /**
     * 获取商品的所有材质
     *
     * @return
     */
    List<MaterialBean> listAllMaterial();

    /**
     * 入库
     */
    Map<String, Object> addGoods2Store(AddGoods2StoreReq goods);

    /**
     * 出库
     */
    Map<String, Object> outGoodsFormStore(OutGoodsFormStoreReq req);

    /**
     * 获取库存列表
     */
    List<Map<String, Object>> listStoreBySelect(ListGoodsBySelectReq req);

    /**
     * 通过 goodsId 查找商品
     */
    Map<String, Object> findGoodsById(Integer goodsId);

    /**
     * 通过筛选获取出库记录
     */
    List<Map<String, Object>> listAllOutRecordBySelect(ListOutGoodsRecordReq req);

    /**
     * 更新支付状态
     */
    Boolean updatePayStatusByOutId(UpdatePayStatusInOutRecordReq req);
}
