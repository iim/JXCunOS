package mm.tm.jxcos.service;

import mm.tm.jxcos.bean.ColorBean;

/**
 * Descriptions：
 * <p>
 * Author：ChenME
 * Date：2018/12/21
 * Email：ibelieve1210@163.com
 */
public interface ColorService {

    /**
     * 更新颜色
     *
     * @return
     */
    Boolean updateColor(ColorBean bean);

    /**
     * 新增一个颜色
     */
    ColorBean addColor(ColorBean bean);
}
