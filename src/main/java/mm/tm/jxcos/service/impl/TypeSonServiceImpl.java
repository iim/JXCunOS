package mm.tm.jxcos.service.impl;

import mm.tm.jxcos.bean.TypeSonBean;
import mm.tm.jxcos.dao.TypeSonDao;
import mm.tm.jxcos.service.TypeSonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Descriptions：
 * <p>
 * Author：ChenME
 * Date：2018/12/3
 * Email：ibelieve1210@163.com
 */

@Service
public class TypeSonServiceImpl implements TypeSonService {


    @Autowired
    TypeSonDao typeSonDao;

    @Transactional
    @Override
    public Boolean updateTypeSon(TypeSonBean bean) {

        try {
            if (typeSonDao.updateTypeSon(bean) > 0) {
                return true;
            } else {
                throw new RuntimeException("类型更新失败！");
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("类型更新失败！");
        }

    }

    @Transactional
    @Override
    public TypeSonBean addTypeSon(TypeSonBean bean) {

        try {
            if (typeSonDao.addTypeSon(bean) > 0) {
                return bean;
            } else {
                throw new RuntimeException("类型新增失败！");
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("类型新增失败！");
        }
    }
}
