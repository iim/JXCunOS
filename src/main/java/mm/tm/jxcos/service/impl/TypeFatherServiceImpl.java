package mm.tm.jxcos.service.impl;

import mm.tm.jxcos.bean.TypeFatherBean;
import mm.tm.jxcos.dao.TypeFatherDao;
import mm.tm.jxcos.service.TypeFatherService;
import mm.tm.jxcos.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Descriptions：
 * <p>
 * Author：ChenME
 * Date：2018/11/23
 * Email：ibelieve1210@163.com
 */
@Service
public class TypeFatherServiceImpl implements TypeFatherService {

    @Autowired
    TypeFatherDao mDao;

    @Override
    public List<TypeFatherBean> listAllTypeFather() {
        return mDao.listAllTypeFather();
    }

    @Override
    public TypeFatherBean findTypeFatherById(Integer id) {
        return mDao.findTypeFatherById(id);
    }

    @Transactional // 开启事务
    @Override
    public TypeFatherBean addTypeFather(TypeFatherBean typeFatherBean) {
        System.out.println(StringUtils.isEmpty(typeFatherBean.getTypeFatherName()));
        if (!StringUtils.isEmpty(typeFatherBean.getTypeFatherName())) {
            try {
                int lines = mDao.addTypeFather(typeFatherBean);
                if (lines > 0) {
                    return typeFatherBean;
                } else {
                    throw new RuntimeException("增加类型信息失败！");
                }
            } catch (Exception e) {
                throw new RuntimeException("增加类型信息失败！-> [" + e.getMessage() + "]");
            }
        } else {
            throw new RuntimeException("类型名称不能为空！");
        }
    }

    @Transactional
    @Override
    public Boolean updateTypeFather(TypeFatherBean typeFatherBean) {
        if (!StringUtils.isEmpty(typeFatherBean.getTypeFatherName())) {
            try {
                int lines = mDao.updateTypeFather(typeFatherBean);
                if (lines > 0) {
                    return true;
                } else {
                    throw new RuntimeException("更新类型信息失败！");
                }
            } catch (Exception e) {
                throw new RuntimeException("更新类型信息失败！-> [" + e.getMessage() + "]");
            }
        } else {
            throw new RuntimeException("类型名称不能为空！");
        }
    }

    @Transactional
    @Override
    public Boolean delTypeFather(Integer id) {
        try {
            int lines = mDao.delTypeFather(id);
            if (lines > 0) {
                return true;
            } else {
                throw new RuntimeException("删除类型信息失败！");
            }
        } catch (Exception e) {
            throw new RuntimeException("删除类型信息失败！-> [" + e.getMessage() + "]");
        }
    }
}
