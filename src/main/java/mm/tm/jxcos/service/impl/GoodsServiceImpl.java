package mm.tm.jxcos.service.impl;

import mm.tm.jxcos.bean.*;
import mm.tm.jxcos.dao.*;
import mm.tm.jxcos.domain.*;
import mm.tm.jxcos.service.GoodsService;
import mm.tm.jxcos.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Descriptions：
 * <p>
 * Author：ChenME
 * Date：2018/11/23
 * Email：ibelieve1210@163.com
 */
@Service
public class GoodsServiceImpl implements GoodsService {


    @Autowired
    TypeSonDao typeSonDao;

    @Autowired
    ColorDao colorDao;

    @Autowired
    SizeDao sizeDao;

    @Autowired
    MaterialDao materialDao;

    @Autowired
    GoodsDao goodsDao;

    @Autowired
    InGoodsDao inGoodsDao;

    @Autowired
    StoreDao storeDao;

    @Autowired
    OutGoodsDao outGoodsDao;

    @Override
    public List<TypeSonBean> listTypeSonByFatherId(Integer typeFatherId) {
        return typeSonDao.listTypeSonByFatherId(typeFatherId);
    }

    @Override
    public List<ColorBean> listAllColor() {
        return colorDao.listAllColor();
    }

    @Override
    public List<SizeBean> listSizeByTypeFatherId(Integer typeFatherId) {
        return sizeDao.listSizeByTypeFatherId(typeFatherId);
    }

    @Override
    public List<MaterialBean> listAllMaterial() {
        return materialDao.listAllMaterial();
    }

    @Transactional
    @Override
    public HashMap<String, Object> addGoods2Store(AddGoods2StoreReq req) {
        HashMap<String, Object> resp = new HashMap<>();

        GoodsBean goods = new GoodsBean(0, req.getName(), req.getGoodsNumber(), req.getTypeFather(), req.getType(), req.getColor(), req.getSize(), req.getMaterial(), req.getGender(), req.getPriceBuying(), req.getPriceGuidance(), req.getDescription(), req.getFromBusiness(), req.getRemark());
        int linesGoods = goodsDao.addGoods(goods);
        if (linesGoods > 0) { // 插入商品成功
            resp.put("id", goods.getId());

            InGoodsBean inGoods = new InGoodsBean(0, goods.getId(), new Date(), req.getInNumber(), req.getRemark());
            int linesStoreRecord = inGoodsDao.addInGoods(inGoods);
            if (linesStoreRecord > 0) { // 插入入库记录成功
                resp.put("inId", inGoods.getInId());

                StoreBean store = new StoreBean(goods.getId(), req.getInNumber(), new Date(), new Date());
                int linesStore = storeDao.addStore(store);
                if (linesStore > 0) { // 插入库存记录成功
                    return resp;
                } else {
                    throw new RuntimeException("更新库存记录失败！");
                }

//                StoreBean store = storeDao.findStoreByGoodsId(goods.getId());
//                if (null == store) { // 暂无库存记录
//                    store = new StoreBean(goods.getId(), req.getInNumber());
//                    int linesStore = storeDao.addStore(store);
//                    if (linesStore > 0) { // 插入库存记录成功
//                        return resp;
//                    } else {
//                        throw new RuntimeException("更新库存记录失败！");
//                    }
//                }else{ // 有库存记录
//                    store.setCountLeft(req.getInNumber());
//                    int linesStore = storeDao.updateStore(store);
//                    if (linesStore > 0) { // 更新库存记录成功
//                        return resp;
//                    } else {
//                        throw new RuntimeException("更新库存记录失败！");
//                    }
//                }
            } else {
                throw new RuntimeException("插入入库记录失败！");
            }
        } else {
            throw new RuntimeException("插入商品信息失败！");
        }
    }

    @Override
    public List<Map<String, Object>> listStoreBySelect(ListGoodsBySelectReq req) {
        try {
            List<Map<String, Object>> resp = goodsDao.listStoreBySelect(req);
            for (Map<String, Object> aResp : resp) {
                aResp.put("createTime", DateUtils.formatDate((Date) aResp.get("createTime")));
                aResp.put("lastModifyTime", DateUtils.formatDate((Date) aResp.get("lastModifyTime")));
            }
            return resp;
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("查询失败！");
        }
    }


    @Transactional
    @Override
    public Map<String, Object> outGoodsFormStore(OutGoodsFormStoreReq req) {
        try {
            OutGoodsBean outGoods = new OutGoodsBean(0, req.getGoodsId(), new Date(), req.getOutNumber(), req.getPriceOut(), req.getPayStatus(), new Date(), req.getOutRemark());
            int linesOut = outGoodsDao.addOutRecord(outGoods);
            if (linesOut > 0) { // 出库记录插入成功
                StoreBean store = storeDao.findStoreByGoodsId(req.getGoodsId());
                if (null != store) { // 查询库存成功
                    store.setCountLeft(store.getCountLeft() - req.getOutNumber());
                    store.setLastModifyTime(new Date());
                    int linesStore = storeDao.updateStore(store);
                    if (linesStore > 0) { // 更新库存记录成功
                        Map<String, Object> resp = new HashMap<>();
                        resp.put("outId", outGoods.getOutId());
                        resp.put("goodsId", store.getGoodsId());
                        return resp;
                    } else {
                        throw new RuntimeException("更新库存记录失败！");
                    }

                } else {
                    throw new RuntimeException("未查询到该商品的库存信息！");
                }
            } else {
                throw new RuntimeException("出库失败！");
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("出库失败！");
        }
    }

    @Override
    public Map<String, Object> findGoodsById(Integer goodsId) {
        try {
            Map<String, Object> resp = goodsDao.findGoodsById(goodsId);
            resp.put("createTime", DateUtils.formatDate((Date) resp.get("createTime")));
            resp.put("lastModifyTime", DateUtils.formatDate((Date) resp.get("lastModifyTime")));
            return resp;
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("未找到指定ID的商品！");
        }
    }

    @Override
    public List<Map<String, Object>> listAllOutRecordBySelect(ListOutGoodsRecordReq req) {
        List<Map<String, Object>> respList = outGoodsDao.listAllOutRecordBySelect(req);
        for (Map<String, Object> resp : respList) {
            resp.put("outTime", DateUtils.formatDate((Date) resp.get("outTime")));
            resp.put("lastModifyTime", DateUtils.formatDate((Date) resp.get("lastModifyTime")));
        }
        return respList;
    }

    @Transactional
    @Override
    public Boolean updatePayStatusByOutId(UpdatePayStatusInOutRecordReq req) {
        req.setLastModifyTime(new Date());
        Integer resp = outGoodsDao.updatePayStatusByOutId(req);
        return resp > 0;
    }
}
