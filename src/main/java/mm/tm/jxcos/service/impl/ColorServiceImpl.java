package mm.tm.jxcos.service.impl;

import mm.tm.jxcos.bean.ColorBean;
import mm.tm.jxcos.dao.ColorDao;
import mm.tm.jxcos.service.ColorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Descriptions：
 * <p>
 * Author：ChenME
 * Date：2018/12/3
 * Email：ibelieve1210@163.com
 */

@Service
public class ColorServiceImpl implements ColorService {


    @Autowired
    ColorDao colorDao;

    @Transactional
    @Override
    public Boolean updateColor(ColorBean bean) {
        try {
            if (colorDao.updateColor(bean) > 0) {
                return true;
            } else {
                throw new RuntimeException("颜色信息更新失败！");
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("颜色信息更新失败！");
        }
    }

    @Transactional
    @Override
    public ColorBean addColor(ColorBean bean) {
        try {
            if (colorDao.addColor(bean) > 0) {
                return bean;
            } else {
                throw new RuntimeException("新增颜色失败！");
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("新增颜色失败！");
        }
    }
}
