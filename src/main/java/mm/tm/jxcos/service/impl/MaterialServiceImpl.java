package mm.tm.jxcos.service.impl;

import mm.tm.jxcos.bean.MaterialBean;
import mm.tm.jxcos.dao.MaterialDao;
import mm.tm.jxcos.service.MaterialService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Descriptions：
 * <p>
 * Author：ChenME
 * Date：2018/12/3
 * Email：ibelieve1210@163.com
 */

@Service
public class MaterialServiceImpl implements MaterialService {


    @Autowired
    MaterialDao materialDao;

    @Transactional
    @Override
    public Boolean updateMaterial(MaterialBean bean) {
        try {
            if (materialDao.updateMaterial(bean) > 0) {
                return true;
            } else {
                throw new RuntimeException("材质信息更新失败！");
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("材质信息更新失败！");
        }
    }

    @Transactional
    @Override
    public MaterialBean addMaterial( MaterialBean bean) {
        try {
            if (materialDao.addMaterial(bean) > 0) {
                return bean;
            } else {
                throw new RuntimeException("新增材质失败！");
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("新增材质失败！");
        }
    }
}
