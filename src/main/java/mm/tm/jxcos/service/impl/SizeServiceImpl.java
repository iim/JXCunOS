package mm.tm.jxcos.service.impl;

import mm.tm.jxcos.bean.SizeBean;
import mm.tm.jxcos.dao.SizeDao;
import mm.tm.jxcos.service.SizeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Descriptions：
 * <p>
 * Author：ChenME
 * Date：2018/12/3
 * Email：ibelieve1210@163.com
 */

@Service
public class SizeServiceImpl implements SizeService {


    @Autowired
    SizeDao sizeDao;

    @Transactional
    @Override
    public Boolean updateSize(SizeBean bean) {
        try {
            if (sizeDao.updateSize(bean) > 0) {
                return true;
            } else {
                throw new RuntimeException("尺寸信息失败！");
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("尺寸信息失败！");
        }
    }

    @Transactional
    @Override
    public SizeBean addSize(SizeBean bean) {
        try {
            if (sizeDao.addSize(bean) > 0) {
                return bean;
            } else {
                throw new RuntimeException("新增尺寸失败！");
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("新增尺寸失败！");
        }
    }
}
