package mm.tm.jxcos.service;

import mm.tm.jxcos.bean.TypeSonBean;

/**
 * Descriptions：
 * <p>
 * Author：ChenME
 * Date：2018/12/21
 * Email：ibelieve1210@163.com
 */
public interface TypeSonService {

    /**
     * 更新小类型信息
     *
     * @return
     */
    Boolean updateTypeSon(TypeSonBean bean);


    /**
     * 新增一个类型
     */
    TypeSonBean addTypeSon(TypeSonBean bean);
}