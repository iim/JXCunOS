package mm.tm.jxcos.service;

import mm.tm.jxcos.bean.SizeBean;

/**
 * Descriptions：
 * <p>
 * Author：ChenME
 * Date：2018/12/21
 * Email：ibelieve1210@163.com
 */
public interface SizeService {

    /**
     * 更新尺寸
     *
     * @return
     */
    Boolean updateSize(SizeBean bean);


    /**
     * 新增一个尺寸
     */
    SizeBean addSize(SizeBean bean);
}