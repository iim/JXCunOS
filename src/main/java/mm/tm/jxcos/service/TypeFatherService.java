package mm.tm.jxcos.service;

import mm.tm.jxcos.bean.TypeFatherBean;

import java.util.List;

/**
 * Descriptions：
 * <p>
 * Author：ChenME
 * Date：2018/12/21
 * Email：ibelieve1210@163.com
 */
public interface TypeFatherService {

    /**
     * 获取所有的大类型
     */
    List<TypeFatherBean> listAllTypeFather();

    /**
     * 通过大类型的 ID 获取大类型
     */
    TypeFatherBean findTypeFatherById(Integer id);

    /**
     * 添加一个大类型
     */
    TypeFatherBean addTypeFather(TypeFatherBean typeFatherBean);

    /**
     * 更新一个大类型
     */
    Boolean updateTypeFather(TypeFatherBean typeFatherBean);

    /**
     * 删除一个大类型
     */
    Boolean delTypeFather(Integer id);
}