package mm.tm.jxcos.config.dao;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import mm.tm.jxcos.constant.Const;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.beans.PropertyVetoException;

/**
 * Descriptions：数据库配置
 * <p>
 * Author：ChenME
 * Date：2018/11/26
 * Email：ibelieve1210@163.com
 */

@Configuration
@MapperScan("mm.tm.jxcos.dao") // 配置 mybatis mapper 的扫描路径
public class DataSourceConfiguration {

    @Value("${jdbc.driver}")
    private String driverClass;
    @Value("${jdbc.url}")
    private String jdbcUrl;
    @Value("${jdbc.url_test}")
    private String jdbcTestUrl;
    @Value("${jdbc.user}")
    private String jdbcUser;
    @Value("${jdbc.pwd}")
    private String jdbcPwd;

    @Bean(name = "dataSource")
    public ComboPooledDataSource createDataSource() throws PropertyVetoException {

        ComboPooledDataSource dataSource = new ComboPooledDataSource();
        dataSource.setDriverClass(driverClass);
        if (Const.IsDebug) {
            dataSource.setJdbcUrl(jdbcTestUrl);
        } else {
            dataSource.setJdbcUrl(jdbcUrl);
        }
        dataSource.setUser(jdbcUser);
        dataSource.setPassword(jdbcPwd);

        //关闭连接后不自动 commit，便于事务控制
        dataSource.setAutoCommitOnClose(false);

        return dataSource;
    }
}
