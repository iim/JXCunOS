package mm.tm.jxcos.utils;

import java.util.Date;
import java.text.SimpleDateFormat;

/**
 * Descriptions：
 * <p>
 * Author：ChenME
 * Date：2018/12/21
 * Email：ibelieve1210@163.com
 */
public class DateUtils {
    public static String formatDate(Date date, DateFormatType formatType) {
        return new SimpleDateFormat(formatType.getFormatType()).format(date);
    }

    public static String formatDate(Date date) {
        return formatDate(date, DateFormatType.AndTimeDefault);
    }

    enum DateFormatType {
        AndTimeDefault("yyyy/MM/dd HH:mm:ss"), // 2018/08/08 08:08:08
        AndTimeDefaultWithoutSecond("yyyy/MM/dd HH:mm"), // 2018/08/08 08:08
        AndTimeSimple("yy/M/d H:mm:ss"), // 18/8/8 8:08:08
        AndTimeSimpleWithoutSecond("yy/M/d H:mm"), // 18/8/8 8:08
        WithoutTimeDefault("yyyy/MM/dd"), // 2018/08/08
        WithoutTimeSimple("yy/M/d"); // 18/8/8


        private String formatType;

        public String getFormatType() {
            return formatType;
        }

        DateFormatType(String formatType) {
            this.formatType = formatType;
        }
    }
}
