package mm.tm.jxcos.utils;

/**
 * Descriptions：
 * <p>
 * Author：ChenME
 * Date：2018/11/27
 * Email：ibelieve1210@163.com
 */
public class StringUtils {

    public static boolean isEmpty(String... arg) {
        for (String it : arg) {
            boolean res = null == it || "".equals(it) || "null".equals(it.toLowerCase());
            if (res) {
                return true;
            }
        }
        return false;
    }
}
