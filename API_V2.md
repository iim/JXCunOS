## 进销存管理 OS API 文档 V2

---

### Header



|key           |value   |
|------------- |-----------------|
|Content-Type  |application/json |

---

### 1. 更新类型（小类型）
#### 1. method： `POST`
#### 2. action：`type/updateTypeSon`
#### 3. param：

|param           |必传   |说明     |
|-------------   |----- |------- |
|typeId          |Y     |要修改类型的 ID |
|typeFatherId    |Y     |大类型的 ID（1 上衣，2 裤子） |
|typeName        |Y     |类型的名称 |
|typeDescription |N     |类型的描述（数据库是 ***覆盖存储*** ，如果之前有备注信息，前台需要将之前的备注信息以及新的备注信息 ***拼接之后*** 传到后台） |
|typeRemark      |N     |类型的备注（数据库是 ***覆盖存储*** ，如果之前有备注信息，前台需要将之前的备注信息以及新的备注信息 ***拼接之后*** 传到后台） |

#### 4. demo：
+ URL：`http://localhost:8082/jxcos/type/updateTypeSon`
+ param：

```
{
    "typeId":15,
    "typeFatherId":2,
    "typeName":"皮裙",
    "typeDescription":"皮裙",
    "typeRemark":"皮裙"
}
```

+ resp:

```
{
    "status": 0,
    "msg": "成功",
    "data": null
}
```

---

### 2.  新增类型（小类型）
#### 1. method： `POST`
#### 2. action：`type/addTypeSon`
#### 3. param：

|param           |必传   |说明     |
|-------------   |----- |------- |
|typeFatherId    |Y     |大类型的 ID（1 上衣，2 裤子） |
|typeName        |Y     |类型的名称 |
|typeDescription |N     |类型的描述 |
|typeRemark      |N     |类型的备注 |

#### 4. demo：
+ URL：`http://localhost:8082/jxcos/type/addTypeSon`
+ param：

```
{
    "typeFatherId":1,
    "typeName":"针织衫",
    "typeDescription":"针织衫",
    "typeRemark":"针织衫"
}
```

+ resp：

```
{
    "status": 0,
    "msg": "成功",
    "data": {
        "typeId": 19,
        "typeFatherId": 1,
        "typeName": "针织衫",
        "typeDescription": "针织衫",
        "typeRemark": "针织衫"
    }
}
```

---

### 3. 更新颜色
#### 1. method： `POST`
#### 2. action：`color/updateColor`
#### 3. param：

|param           |必传   |说明     |
|-------------   |----- |------- |
|colorId          |Y     |要修改颜色的 ID |
|colorName        |Y     |颜色的名称 |
|colorDescription |N     |颜色的描述（数据库是 ***覆盖存储*** ，如果之前有备注信息，前台需要将之前的备注信息以及新的备注信息 ***拼接之后*** 传到后台） |
|colorRemark      |N     |颜色的备注（数据库是 ***覆盖存储*** ，如果之前有备注信息，前台需要将之前的备注信息以及新的备注信息 ***拼接之后*** 传到后台） |

#### 4. demo：
+ URL：`http://localhost:8082/jxcos/color/updateColor`
+ param：

```
{
    "colorId":10,
    "colorName":"无色透明",
    "colorDescription":"皇帝的新衣",
    "colorRemark":"皇帝的新衣"
}
```

+ resp:

```
{
    "status": 0,
    "msg": "成功",
    "data": null
}
```

---

### 4.  新增颜色
#### 1. method： `POST`
#### 2. action：`color/addColor`
#### 3. param：

|param           |必传   |说明     |
|-------------   |----- |------- |
|colorName        |Y     |颜色的名称 |
|colorDescription |N     |颜色的描述 |
|colorRemark      |N     |颜色的备注 |

#### 4. demo：
+ URL：`http://localhost:8082/jxcos/color/addColor`
+ param：

```
{
    "colorName":"无色",
    "colorDescription":"就是这么任性",
    "colorRemark":"就是这么任性"
}
```

+ resp：

```
{
    "status": 0,
    "msg": "成功",
    "data": {
        "colorId": 10,
        "colorName": "无色",
        "colorDescription": "就是这么任性",
        "colorRemark": "就是这么任性"
    }
}
```


---

### 5. 更新材质
#### 1. method： `POST`
#### 2. action：`material/updateMaterial`
#### 3. param：

|param               |必传   |说明     |
|-------------       |----- |------- |
|materialId          |Y     |要修改材质的 ID |
|materialName        |Y     |材质的名称 |
|materialDescription |N     |材质的描述（数据库是 ***覆盖存储*** ，如果之前有备注信息，前台需要将之前的备注信息以及新的备注信息 ***拼接之后*** 传到后台） |
|materialRemark      |N     |材质的备注（数据库是 ***覆盖存储*** ，如果之前有备注信息，前台需要将之前的备注信息以及新的备注信息 ***拼接之后*** 传到后台） |

#### 4. demo：
+ URL：`http://localhost:8082/jxcos/material/updateMaterial`
+ param：

```
{
    "materialId": 9,
    "materialName":"貂皮狐狸皮",
    "materialDescription":"就是这么任性",
    "materialRemark":"就是这么任性"
}
```

+ resp:

```
{
    "status": 0,
    "msg": "成功",
    "data": null
}
```

---

### 6.  新增材质
#### 1. method： `POST`
#### 2. action：`material/addMaterial`
#### 3. param：

|param               |必传   |说明     |
|-------------       |----- |------- |
|materialName        |Y     |材质的名称 |
|materialDescription |N     |材质的描述 |
|materialRemark      |N     |材质的备注 |

#### 4. demo：
+ URL：`http://localhost:8082/jxcos/material/addMaterial`
+ param：

```
{
    "materialName":"貂皮",
    "materialDescription":"就是这么任性",
    "materialRemark":"就是这么任性"
}
```

+ resp：

```
{
    "status": 0,
    "msg": "成功",
    "data": {
        "materialId": 9,
        "materialName": "貂皮",
        "materialDescription": "就是这么任性",
        "materialRemark": "就是这么任性"
    }
}
```

---

### 7. 更新尺寸
#### 1. method： `POST`
#### 2. action：`size/updateSize`
#### 3. param：

|param           |必传   |说明     |
|-------------   |----- |------- |
|sizeId          |Y     |要修改尺寸的 ID |
|typeFatherId    |Y     |大类型的 ID（1 上衣，2 裤子） |
|sizeName        |Y     |尺寸的名称 |
|sizeDescription |N     |尺寸的描述（数据库是 ***覆盖存储*** ，如果之前有备注信息，前台需要将之前的备注信息以及新的备注信息 ***拼接之后*** 传到后台） |
|sizeRemark      |N     |尺寸的备注（数据库是 ***覆盖存储*** ，如果之前有备注信息，前台需要将之前的备注信息以及新的备注信息 ***拼接之后*** 传到后台） |

#### 4. demo：
+ URL：`http://localhost:8082/jxcos/size/updateSize`
+ param：

```
{
    "sizeId":2,
    "typeFatherId":2,
    "sizeName":"2尺1?",
    "sizeDescription":"小蛮腰?",
    "sizeRemark":"小蛮腰?"
}
```

+ resp:

```
{
    "status": 0,
    "msg": "成功",
    "data": null
}
```

---

### 8.  新增尺寸
#### 1. method： `POST`
#### 2. action：`size/addSize`
#### 3. param：

|param           |必传   |说明     |
|-------------   |----- |------- |
|typeFatherId    |Y     |大类型的 ID（1 上衣，2 裤子） |
|sizeName        |Y     |尺寸的名称 |
|sizeDescription |N     |尺寸的描述 |
|sizeRemark      |N     |尺寸的备注 |

#### 4. demo：
+ URL：`http://localhost:8082/jxcos/size/addSize`
+ param：

```
{
    "typeFatherId":2,
    "sizeName":"2尺1",
    "sizeDescription":"小蛮腰",
    "sizeRemark":"小蛮腰"
}
```

+ resp：

```
{
    "status": 0,
    "msg": "成功",
    "data": {
        "sizeId": 2,
        "typeFatherId": 2,
        "sizeName": "2尺1",
        "sizeDescription": "小蛮腰",
        "sizeRemark": "小蛮腰"
    }
}
```

---