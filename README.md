# JXCunOS

---

#### 项目介绍
进销存系统后台

---

#### API 文档
1. [API 文档 V1](https://gitee.com/iim/JXCunOS/blob/master/API_V1.md)
2. [API 文档 V2](https://gitee.com/iim/JXCunOS/blob/master/API_V2.md)

---

有关版本控制

| 版本号          | 说明      |
|----------------|----------|
| V1_UseKotlin   | 该分支用了 kotlin 的代码，由于打包异常，所以从改版之后，将抛弃 kotlin 部分，改为纯 Java 开发  |

