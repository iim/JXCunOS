## 进销存管理 OS API 文档 V1

---

### Header



|key           |value   |
|------------- |-----------------|
|Content-Type  |application/json |

---

### 1. 根据商品的父类型 ID 获取商品的子类型类型（毛衣、外套）
#### 1. method： `GET`
#### 2. action：`goods/listTypeSonByFatherId`
#### 3. param：

|param         |必传   |说明     |
|------------- |----- |------- |
|typeFatherId  |Y     |1 上衣，2 裤子 |

#### 4. demo：
+ URL：`http://localhost:8082/jxcos/goods/listTypeSonByFatherId?typeFatherId=2`
+ resp:

```
{
    "status": 0,
    "msg": "成功",
    "data": {
        "list": [
            {
                "typeId": 4,
                "typeFatherId": 2,
                "typeName": "阔腿裤",
                "typeDescription": "",
                "typeRemark": null
            },
            {
                "typeId": 5,
                "typeFatherId": 2,
                "typeName": "短裙",
                "typeDescription": "",
                "typeRemark": null
            }
        ]
    }
}
```

---

### 2. 获取商品的所有颜色
#### 1. method： `GET`
#### 2. action：`goods/listAllColor`
#### 3. param：无
#### 4. demo：
+ URL：`http://localhost:8082/jxcos/goods/listAllColor`
+ resp：

```
{
    "status": 0,
    "msg": "成功",
    "data": {
        "list": [
            {
                "colorId": 1,
                "colorName": "杂色",
                "colorDescription": null,
                "colorRemark": null
            },
            {
                "colorId": 2,
                "colorName": "黑色",
                "colorDescription": null,
                "colorRemark": null
            }
        ]
    }
}
```

---

### 3. 根据商品的大类型获取商品的尺寸
#### 1. method： `GET`
#### 2. action：`goods/listSizeByTypeFatherId`
#### 3. param：

|param         |必传   |说明     |
|------------- |----- |------- |
|typeFatherId  |Y     |1 上衣，2 裤子 |

#### 4. demo：
+ URL：`http://localhost:8082/jxcos/goods/listSizeByTypeFatherId?typeFatherId=1`
+ resp：

```
{
    "status": 0,
    "msg": "成功",
    "data": {
        "list": [
            {
                "sizeId": 1,
                "typeFatherId": 1,
                "sizeName": "F",
                "sizeDescription": "均码",
                "sizeRemark": null
            }
        ]
    }
}
```

---

### 4. 获取商品的所有材质
#### 1. method： `GET`
#### 2. action：`goods/listAllMaterial`
#### 3. param：无
#### 4. demo：
+ URL：`http://localhost:8082/jxcos/goods/listAllMaterial`
+ resp：

```
{
    "status": 0,
    "msg": "成功",
    "data": {
        "list": [
            {
                "materialId": 1,
                "materialName": "棉",
                "materialDescription": null,
                "materialRemark": null
            },
            {
                "materialId": 2,
                "materialName": "丝",
                "materialDescription": null,
                "materialRemark": null
            }
        ]
    }
}
```

---

### 5. 入库操作
#### 1. method： `POST`
#### 2. action：`goods/addGoods2Store`
#### 3. param：

|param          |必传   |说明        |
|---------------|----- |---------- |
|name           |Y     |商品名称 |
|goodsNumber    |Y     |货号（格式：181128001） |
|typeFather     |Y     |大类型 ID（1 上衣，2 裤子） |
|type           |Y     |小类型 ID |
|color          |Y     |颜色 ID |
|size           |Y     |尺寸 ID |
|material       |Y     |材质 ID |
|gender         |Y     |男女款 ID（1 中性，2 男款，3 女款） |
|priceBuying    |Y     |进货价，单位分 |
|priceGuidance  |N     |指导价，单位分 |
|description    |N     |描述信息 |
|fromBusiness   |N     |进货渠道 |
|remark         |N     |备注 |
|inNumber       |Y     |入库件数 |

#### 4. demo：
+ URL：`http://localhost:8082/jxcos/goods/addGoods2Store`
+ param：

```
{
    "name":"测试数据",
    "goodsNumber":"181128002",
    "typeFather":1,
    "type":2,
    "color":2,
    "size":1,
    "material":2,
    "gender":1,
    "priceBuying":3000,
    "priceGuidance":6000,
    "description":"",
    "fromBusiness":"多又多",
    "remark":"XXX",
    "inNumber":10
}
```

+ resp：

```
{
    "status": 0,
    "msg": "成功",
    "data": {
        "inId": 1,
        "id": 5
    }
}
```

---

### 6. 获取库存列表
#### 1. method： `POST`
#### 2. action：`goods/listStoreBySelect`
#### 3. param：

|param          |必传   |说明        |
|---------------|----- |---------- |
|goodsNumber    |N     |货号（格式：181128001），筛选全部库存时：不传（或者传 `null`)，***不可传空字符串，否则筛选不到任何信息*** |
|colorId        |N     |颜色 ID，筛选全部库存时：不传（或者传 `0`) |
|sizeId         |N     |尺寸 ID，筛选全部库存时：不传（或者传 `0`) |
|startIndex     |N     |数据开始索引（分页用，不传时默认为 `0`） |
|pageSize       |N     |每页长度（分页用，不传时默认为 `20`） |

#### 4. demo：
+ URL：`http://localhost:8082/jxcos/goods/listStoreBySelect`
+ param

```
{
  "goodsNumber": "181130002",
  "colorId": 2,
  "sizeId": 1,
  "startIndex": 0,
  "pageSize": 4
}
```

+ resp：

```
{
    "status": 0,
    "msg": "成功",
    "data": {
        "list": [
            {
                "colorName": "黑色",
                "priceBuying": 3000,
                "color": 2,
                "goodsId": 6,
                "typeName": "打底衫",
                "priceGuidance": 6000,
                "type": 2,
                "size": 1,
                "sizeName": "F",
                "createTime": "2018/11/30 10:27:28",
                "goodsNumber": "181130002",
                "lastModifyTime": "2018/12/03 14:05:59",
                "countLeft": 9,
                "goodsName": "测试数据2"
            }
        ]
    }
}
```

---

### 7. 出库
#### 1. method： `POST`
#### 2. action：`goods/outGoodsFormStore`
#### 3. param：

|param       |必传   |说明        |
|------------|----- |---------- |
|goodsId     |Y     |商品 ID     |
|outNumber   |Y     |出库数量    |
|priceOut    |Y     |实际售价    |
|payStatus   |Y     |支付状态（1 待支付，2 已支付） |
|outRemark   |N     |备注信息    |

#### 4. demo：
+ URL：`http://localhost:8082/jxcos/goods/outGoodsFormStore`
+ param

```
{
  "goodsId":5,
  "outNumber":2,
  "priceOut":7000,
  "payStatus":1,
  "outRemark":""
}
```

+ resp：

```
{
    "status": 0,
    "msg": "成功",
    "data": {
        "goodsId": 5,
        "outId": 5
    }
}
```

---

### 8. 通过 goodsId 查找商品
#### 1. method： `GET`
#### 2. action：`goods/findGoodsById`
#### 3. param：

|param       |必传   |说明        |
|------------|----- |---------- |
|goodsId     |Y     |商品 ID     |

#### 4. demo：
+ URL：`http://localhost:8082/jxcos/goods/findGoodsById?goodsId=6`
+ resp：

```
{
    "status": 0,
    "msg": "成功",
    "data": {
        "colorName": "黑色",
        "priceBuying": 3000,
        "color": 2,
        "gender": 1,
        "goodsId": 6,
        "typeName": "打底衫",
        "description": "",
        "remark": "XXX",
        "priceGuidance": 6000,
        "type": 2,
        "genderName": "中性",
        "typeFather": 1,
        "materialName": "丝",
        "fromBusiness": "多又多",
        "size": 1,
        "material": 2,
        "sizeName": "F",
        "createTime": "2018/11/30 10:27:28",
        "goodsNumber": "181130002",
        "lastModifyTime": "2018/11/30 10:27:28",
        "countLeft": 10,
        "typeFatherName": "上衣",
        "goodsName": "测试数据2"
    }
}
```

---

### 9. 通过筛选获取出库记录
#### 1. method： `POST`
#### 2. action：`goods/listAllOutRecordBySelect`
#### 3. param：

|param       |必传   |说明        |
|------------|----- |---------- |
|payStatus   |Y     |支付状态（0 全部，1 待支付，2 已支付）  |

#### 4. demo：
+ URL：`http://localhost:8082/jxcos/goods/listAllOutRecordBySelect`
+ param：

```
{
    "payStatus":1
}
```

+ resp：

```
{
    "status": 0,
    "msg": "成功",
    "data": {
        "list": [
            {
                "colorName": "黑色",
                "goodsId": 5,
                "typeName": "打底衫",
                "outNumber": 1,
                "sizeName": "F",
                "outRemark": "",
                "lastModifyTime": "2018/12/03 11:00:22",
                "goodsNumber": "181128002",
                "name": "测试数据",
                "outId": 15,
                "priceOut": 7000,
                "payStatus": 1,
                "payName": "待支付",
                "outTime": "2018/12/03 11:00:22"
            },
            {
                "colorName": "黑色",
                "goodsId": 5,
                "typeName": "打底衫",
                "outNumber": 1,
                "sizeName": "F",
                "outRemark": "",
                "lastModifyTime": "2018/12/03 10:48:39",
                "goodsNumber": "181128002",
                "name": "测试数据",
                "outId": 14,
                "priceOut": 7000,
                "payStatus": 1,
                "payName": "待支付",
                "outTime": "2018/12/03 10:48:39"
            }
        ]
    }
}
```

---

### 10. 更新支付状态
#### 1. method： `POST`
#### 2. action：`goods/updatePayStatusByOutId`
#### 3. param：

|param       |必传   |说明        |
|------------|----- |---------- |
|outId       |Y     |出库记录的 ID  |
|payStatus   |Y     |支付状态（1 待支付，2 已支付）  |
|outRemark   |N     |出库的备注信息（数据库是 ***覆盖存储*** ，如果之前有备注信息，前台需要将之前的备注信息以及新的备注信息 ***拼接之后*** 传到后台）  |

#### 4. demo：
+ URL：`http://localhost:8082/jxcos/goods/updatePayStatusByOutId`
+ param：

```
{
    "outId":15,
    "payStatus":2
    "outRemark":"王大锤8月14号才还钱！！"
}
```

+ resp：

```
{
    "status": 0,
    "msg": "成功",
    "data": null
}
```

---
---
---

### 0. 添加总类别
#### 1. method： `POST`
#### 2. action：`type/addTypeFather`
#### 3. param：

|param          |必传   |说明        |
|---------------|----- |---------- |
|typeFatherName |Y     |总类别的名称 |
|typeFatherDescription |N     |对类别的描述信息 |
|typeFatherRemark |N     |备注信息 |

#### 4. demo：
+ URL：`http://localhost:8082/jxcos/type/addTypeFather`
+ param：

```
{
    "typeFatherName":"电视",
    "typeFatherDescription":"电视的总类别，下属可分为：小米电视、飞利浦、海信等",
    "typeFatherRemark":""
}
```

+ resp：

```
{
    "status": 0,
    "msg": "成功",
    "data": {
        "typeFatherId": 10,
        "typeFatherName": "电视",
        "typeFatherDescription": "电视的总类别，下属可分为：小米电视、飞利浦、海信等",
        "typeFatherRemark": ""
    }
}
```