/*
 Navicat MySQL Data Transfer

 Source Server         : CUIDB
 Source Server Type    : MySQL
 Source Server Version : 80012
 Source Host           : localhost:3306
 Source Schema         : jxcos

 Target Server Type    : MySQL
 Target Server Version : 80012
 File Encoding         : 65001

 Date: 25/11/2018 21:53:54
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for goods
-- ----------------------------
DROP TABLE IF EXISTS `goods`;
CREATE TABLE `goods` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '商品名称',
  `goods_number` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '货号',
  `type_father` int(11) NOT NULL COMMENT '商品种类的大类（上衣、裤子）对应goodsTypeFather',
  `type` int(11) NOT NULL COMMENT '商品种类（毛衣帽子等）对应GoodsType表',
  `color` int(11) NOT NULL COMMENT '颜色，对应GoodsColor',
  `size` int(11) NOT NULL COMMENT '尺寸，对应GoodsSize',
  `material` int(11) NOT NULL COMMENT '材质，对应GoodsMaterial',
  `gender` int(11) NOT NULL COMMENT '男女款、中性，对应GoodsGender',
  `price_buying` int(20) NOT NULL COMMENT '进货价（单位：分）',
  `price_guidance` int(20) DEFAULT '0' COMMENT '指导价（单位：分）',
  `description` varchar(255) DEFAULT '' COMMENT '描述',
  `from_business` varchar(50) DEFAULT '' COMMENT '进货渠道',
  `remark` varchar(255) DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`id`),
  KEY `type` (`type`),
  KEY `color` (`color`),
  KEY `size` (`size`),
  KEY `material` (`material`),
  KEY `gender` (`gender`),
  KEY `type_father` (`type_father`),
  CONSTRAINT `goods_ibfk_1` FOREIGN KEY (`type`) REFERENCES `goods_type` (`type_id`),
  CONSTRAINT `goods_ibfk_2` FOREIGN KEY (`color`) REFERENCES `goods_color` (`color_id`),
  CONSTRAINT `goods_ibfk_3` FOREIGN KEY (`size`) REFERENCES `goods_size` (`size_id`),
  CONSTRAINT `goods_ibfk_4` FOREIGN KEY (`material`) REFERENCES `goods_material` (`material_id`),
  CONSTRAINT `goods_ibfk_5` FOREIGN KEY (`gender`) REFERENCES `goods_gender` (`gender_id`),
  CONSTRAINT `goods_ibfk_6` FOREIGN KEY (`type_father`) REFERENCES `goods_type_father` (`type_father_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for goods_color
-- ----------------------------
DROP TABLE IF EXISTS `goods_color`;
CREATE TABLE `goods_color` (
  `color_id` int(11) NOT NULL AUTO_INCREMENT,
  `color_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '颜色名字',
  `color_description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '描述',
  `color_remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`color_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of goods_color
-- ----------------------------
BEGIN;
INSERT INTO `goods_color` VALUES (1, '杂色', NULL, NULL);
INSERT INTO `goods_color` VALUES (2, '黑色', NULL, NULL);
INSERT INTO `goods_color` VALUES (3, '白色', NULL, NULL);
INSERT INTO `goods_color` VALUES (4, '红色', NULL, NULL);
INSERT INTO `goods_color` VALUES (5, '米色', NULL, NULL);
INSERT INTO `goods_color` VALUES (6, '灰色', NULL, NULL);
INSERT INTO `goods_color` VALUES (7, '黄色', NULL, NULL);
INSERT INTO `goods_color` VALUES (9, '蓝色', NULL, NULL);
COMMIT;

-- ----------------------------
-- Table structure for goods_gender
-- ----------------------------
DROP TABLE IF EXISTS `goods_gender`;
CREATE TABLE `goods_gender` (
  `gender_id` int(11) NOT NULL AUTO_INCREMENT,
  `gender_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '男女款式',
  `gender_description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '描述',
  `gender_remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`gender_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of goods_gender
-- ----------------------------
BEGIN;
INSERT INTO `goods_gender` VALUES (1, '中性', '部分男女款', '男女皆可');
INSERT INTO `goods_gender` VALUES (2, '男款', '男款的衣服鞋子等', '只适合男性');
INSERT INTO `goods_gender` VALUES (3, '女款', '女款的衣服鞋子等', '只适合女性');
COMMIT;

-- ----------------------------
-- Table structure for goods_material
-- ----------------------------
DROP TABLE IF EXISTS `goods_material`;
CREATE TABLE `goods_material` (
  `material_id` int(11) NOT NULL AUTO_INCREMENT,
  `material_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '材质名字',
  `material_description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '描述',
  `material_remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`material_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of goods_material
-- ----------------------------
BEGIN;
INSERT INTO `goods_material` VALUES (1, '未知', NULL, NULL);
INSERT INTO `goods_material` VALUES (2, '丝', NULL, NULL);
INSERT INTO `goods_material` VALUES (3, '羽绒', NULL, NULL);
INSERT INTO `goods_material` VALUES (4, '羊毛', NULL, NULL);
INSERT INTO `goods_material` VALUES (5, '涤纶', NULL, NULL);
INSERT INTO `goods_material` VALUES (6, '皮', NULL, NULL);
INSERT INTO `goods_material` VALUES (7, '革', NULL, NULL);
INSERT INTO `goods_material` VALUES (8, '棉', NULL, NULL);
COMMIT;

-- ----------------------------
-- Table structure for goods_size
-- ----------------------------
DROP TABLE IF EXISTS `goods_size`;
CREATE TABLE `goods_size` (
  `size_id` int(11) NOT NULL AUTO_INCREMENT,
  `type_father_id` int(11) NOT NULL COMMENT '商品类别的ID',
  `size_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '尺寸名字',
  `size_description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '描述',
  `size_remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`size_id`) USING BTREE,
  KEY `sTypeId` (`type_father_id`),
  KEY `size_id` (`size_id`),
  CONSTRAINT `goods_size_ibfk_1` FOREIGN KEY (`type_father_id`) REFERENCES `goods_type_father` (`type_father_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of goods_size
-- ----------------------------
BEGIN;
INSERT INTO `goods_size` VALUES (1, 1, 'F', '均码', NULL);
COMMIT;

-- ----------------------------
-- Table structure for goods_type
-- ----------------------------
DROP TABLE IF EXISTS `goods_type`;
CREATE TABLE `goods_type` (
  `type_id` int(11) NOT NULL AUTO_INCREMENT,
  `type_father_id` int(11) NOT NULL COMMENT '所属父类别',
  `type_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '商品种类名字',
  `type_description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '描述',
  `type_remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`type_id`),
  KEY `type_id` (`type_id`),
  KEY `type_id_2` (`type_id`),
  KEY `type_id_3` (`type_id`),
  KEY `type_id_4` (`type_id`),
  KEY `type_father_id` (`type_father_id`),
  CONSTRAINT `goods_type_ibfk_1` FOREIGN KEY (`type_father_id`) REFERENCES `goods_type_father` (`type_father_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of goods_type
-- ----------------------------
BEGIN;
INSERT INTO `goods_type` VALUES (1, 1, '毛衣', '', NULL);
INSERT INTO `goods_type` VALUES (2, 1, '打底衫', '', NULL);
INSERT INTO `goods_type` VALUES (3, 1, '棉上衣', '', NULL);
INSERT INTO `goods_type` VALUES (4, 2, '阔腿裤', '', NULL);
INSERT INTO `goods_type` VALUES (5, 2, '短裙', '', NULL);
INSERT INTO `goods_type` VALUES (6, 2, '长裙 ', '', NULL);
INSERT INTO `goods_type` VALUES (7, 2, '短裤 ', '', NULL);
INSERT INTO `goods_type` VALUES (8, 2, '皮裤', '', NULL);
INSERT INTO `goods_type` VALUES (9, 1, '卫衣 ', '', NULL);
INSERT INTO `goods_type` VALUES (10, 1, '马甲 ', '', NULL);
INSERT INTO `goods_type` VALUES (11, 1, '圆领毛衣', '', NULL);
INSERT INTO `goods_type` VALUES (12, 2, '褶皱裙', '', NULL);
INSERT INTO `goods_type` VALUES (13, 2, '牛仔裤 ', '', NULL);
INSERT INTO `goods_type` VALUES (14, 1, '棉袄', '', NULL);
INSERT INTO `goods_type` VALUES (15, 2, '皮裙', '', NULL);
COMMIT;

-- ----------------------------
-- Table structure for goods_type_father
-- ----------------------------
DROP TABLE IF EXISTS `goods_type_father`;
CREATE TABLE `goods_type_father` (
  `type_father_id` int(11) NOT NULL AUTO_INCREMENT,
  `type_father_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '商品大类的名字（上衣、裤子、鞋子等）',
  `type_father_description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '',
  `type_father_remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '',
  PRIMARY KEY (`type_father_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of goods_type_father
-- ----------------------------
BEGIN;
INSERT INTO `goods_type_father` VALUES (1, '上衣', '上衣、卫衣等', NULL);
INSERT INTO `goods_type_father` VALUES (2, '裤子', '裤子、裙子等', NULL);
COMMIT;

-- ----------------------------
-- Table structure for in_goods
-- ----------------------------
DROP TABLE IF EXISTS `in_goods`;
CREATE TABLE `in_goods` (
  `in_id` int(20) NOT NULL AUTO_INCREMENT,
  `goods_id` int(20) NOT NULL COMMENT '商品ID',
  `in_time` datetime(6) NOT NULL COMMENT '入库时间',
  `in_number` int(11) NOT NULL COMMENT '数量',
  `in_remark` varchar(255) DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`in_id`),
  KEY `goodsId` (`goods_id`),
  CONSTRAINT `in_goods_ibfk_1` FOREIGN KEY (`goods_id`) REFERENCES `goods` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for out_goods
-- ----------------------------
DROP TABLE IF EXISTS `out_goods`;
CREATE TABLE `out_goods` (
  `out_id` int(20) NOT NULL AUTO_INCREMENT,
  `goods_id` int(20) NOT NULL COMMENT '商品ID',
  `out_time` datetime(6) NOT NULL COMMENT '出库时间',
  `out_number` int(11) NOT NULL COMMENT '数量',
  `price_out` int(20) NOT NULL COMMENT '实际售价（单位：分）',
  `pay_status` int(11) NOT NULL COMMENT '支付状态（赊账、已付）对应pay_status',
  `last_modify_time` datetime(6) NOT NULL COMMENT '最近更新时间',
  `out_remark` varchar(255) DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`out_id`),
  KEY `goodsId` (`goods_id`),
  KEY `payStatus` (`pay_status`),
  CONSTRAINT `out_goods_ibfk_1` FOREIGN KEY (`goods_id`) REFERENCES `goods` (`id`),
  CONSTRAINT `out_goods_ibfk_2` FOREIGN KEY (`pay_status`) REFERENCES `pay_status` (`pay_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for pay_status
-- ----------------------------
DROP TABLE IF EXISTS `pay_status`;
CREATE TABLE `pay_status` (
  `pay_id` int(11) NOT NULL AUTO_INCREMENT,
  `pay_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '支付状态名字',
  `pay_description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '描述',
  `pay_remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`pay_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pay_status
-- ----------------------------
BEGIN;
INSERT INTO `pay_status` VALUES (1, '待支付', NULL, NULL);
INSERT INTO `pay_status` VALUES (2, '已支付', NULL, NULL);
COMMIT;

-- ----------------------------
-- Table structure for store
-- ----------------------------
DROP TABLE IF EXISTS `store`;
CREATE TABLE `store` (
  `goods_id` int(20) NOT NULL COMMENT '商品ID对应Goods表中的id字段',
  `count_left` int(20) NOT NULL COMMENT '库存',
  `create_time` datetime(6) NOT NULL COMMENT '创建时间',
  `last_modify_time` datetime(6) NOT NULL COMMENT '最近修改时间',
  PRIMARY KEY (`goods_id`) USING BTREE,
  CONSTRAINT `store_ibfk_1` FOREIGN KEY (`goods_id`) REFERENCES `goods` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


SET FOREIGN_KEY_CHECKS = 1;
